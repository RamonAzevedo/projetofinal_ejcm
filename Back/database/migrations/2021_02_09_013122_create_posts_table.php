<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('posts', function (Blueprint $table) {
            $table->id();
            $table->integer('number_comments')->nullable();
            $table->integer('number_likes')->nullable();
            $table->string('comments')->nullable();
            $table->string('title')->nullable();
            $table->string('text')->nullable();
            $table->string('media')->nullable();
            $table->timestamps();
            /*Post (id <PK>, id_user <FK>, number_comments, number_likes, comments, title, text, media)*/
        });

        Schema::table('posts', function (Blueprint $table){
            $table->foreign('id_user')->references('id')->on("users")->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('posts');
    }
}
