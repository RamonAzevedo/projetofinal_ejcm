<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

class Post extends Model
{
    public function createPost(Request $request){
        $this->number_comments = $request->number_comments;
        $this->number_likes = $request->number_likes;
        $this->comments = $request->comments;
        $this->title = $request->title;
        $this->text = $request->text;
        $this->media = $request->media;
        $this->save();
    }
    public function index() {
    	$posts = Post::all();
    	return response()->json(['posts' => $posts],200);
	}

	public function show($id) {
    	$post = Post::find($id);
    	return response()->json(['post' => $post],200);
	}
    public function update(Request $request, $id){
        $post = Post::find($id);
        if($request->number_comments){
            $post->number_comments = $request->number_comments;
        }
        if($request->number_likes){
            $post->number_likes = $request->number_likes;
        }
        if($request->comments){
            $post->comments = $request->comments;
        }
        if($request->title){
            $post->title = $request->title;
        }
        if($request->text){
            $post->text = $request->text;
        }
        if($request->media){
            $post->media = $request->media;
        }
    }
    public function destroy($id) {
    	Post::destroy($id);
    	return response()->json(['Post deletado com Sucesso!'],200);
	}
    use HasFactory;
}
