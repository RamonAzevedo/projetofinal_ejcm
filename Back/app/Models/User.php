<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Http\Request;

class User extends Authenticatable
{
    /*User (id <PK>, name, email, password, profile_picture, followers, hashtags, identifier, nickname, bio, notifications)*/
    public function createPost(Request $request){
        $this->name = $request->name;
        $this->email = $request->email;
        $this->password = $request->password;
        $this->profile_picture = $request->profile_picture;
        /*$this->hashtags = $request->hashtags;*/
        $this->followers = $request->followers;
        $this->identifier = $request->identifier;
        $this->nickname = $request->nickname;
        $this->bio = $request->bio;
        $this->notifications = $request->notifications;
        $this->save();
    }
    public function index() {
    	$users = User::all();
    	return response()->json(['users' => $users],200);
	}

	public function show($id) {
    	$user = User::find($id);
    	return response()->json(['user' => $user],200);
	}
    public function update(Request $request, $id){
        $user = User::find($id);
        if($request->name){
            $post->name = $request->name;
        }
        if($request->email){
            $post->email = $request->email;
        }
        if($request->password){
            $post->password = $request->password;
        }
        if($request->profile_picture){
            $post->profile_picture = $request->profile_picture;
        }
        if($request->followers){
            $post->followers = $request->followers;
        }
        /*if($request->hashtags){
            $post->hashtags = $request->hashtags;
        }*/
        if($request->identifier){
            $post->identifier = $request->identifier;
        }
        if($request->nickname){
            $post->nickname = $request->nickname;
        }
        if($request->bio){
            $post->bio = $request->bio;
        }
        if($request->notifications){
            $post->notifications = $request->notifications;
        }
    }
    /*User (id <PK>, name, email, password, profile_picture, followers, hashtags, identifier, nickname, bio, notifications)*/
    public function destroy($id) {
    	User::destroy($id);
    	return response()->json(['Usuário deletado com Sucesso!'],200);
	}
    use HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];
}
