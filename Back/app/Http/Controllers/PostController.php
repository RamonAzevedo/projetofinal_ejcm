<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Post;

class PostController extends Controller
{
    public function create(Request $request){
        $post = new Post;
        $post->number_comments = $request->number_comments;
        $post->number_likes = $request->number_likes;
        $post->comments = $request->comments;
        $post->title = $request->title;
        $post->text = $request->text;
        $post->media = $request->media;
        $post->save();
        return response()->json(['post' => $post], 200);

        /*Post (id <PK>, id_user <FK>, number_comments, number_likes, comments, title, text, media)*/
    }

    public function index(){
        $posts = Post::all();
        return response()->json(['post' => $posts], 200);
    }

    public function show($id){
        $post = Post::find($id);
        return response()->json(['post' => $post], 200);
    }

    public function update(Request $request, $id){
        $post = Post::find($id);
        if($request->number_comments){
            $book->number_comments = $request->number_comments;
        }
        if($request->number_likes){
            $book->number_likes = $request->number_likes;
        }
        if($request->comments){
            $book->comments = $request->comments;
        }
        if($request->title){
            $book->title = $request->title;
        }
        if($request->text){
            $book->text = $request->text;
        }
        if($request->media){
            $book->media = $request->media;
        }
    }

    public function destroy($id){
        $post = Post::find($id);
        $post->destroy();
        return response()->json(['Post deletado com sucesso!' => $post], 200);
    }
}
